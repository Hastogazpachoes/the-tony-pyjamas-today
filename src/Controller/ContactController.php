<?php

namespace App\Controller;

use App\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="app_contact")
     * @param Request $request
     * @param \Swift_Mailer $mailer
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function contact(Request $request, \Swift_Mailer $mailer)
    {
        $form = $this->createForm( ContactType::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $contactData = $form->getData();

            $message = (new \Swift_Message('You got mail'))
                ->setFrom($contactData['email'])
                ->setTo('y.siebers@bluetea.nl')
                ->setBody(
                    $contactData['message'],
                    'text/plain'
                );
            $mailer->send($message);

            $this->addFlash('success', 'Message sent!');

            return $this->redirectToRoute('app_contact');
        }
        return $this->render('contact/contact.html.twig',[
            'form' => $form->createView()
        ]);
    }
}
