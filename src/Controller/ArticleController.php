<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Entity\User;
use App\Form\ArticleType;
use App\Form\CommentType;
use App\Repository\ArticleRepository;
use App\Repository\CommentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ArticleController extends AbstractController
{
    /**
     * @Route("/article/", name="article_index", methods="GET")
     *
     * @param ArticleRepository $articleRepository
     * @return Response
     */
    public function index(ArticleRepository $articleRepository): Response
    {
        $articles = $this->getDoctrine()
            ->getRepository(Article::class)->orderByDate();

        return $this->render('article/index.html.twig', ["articles" => $articles]);
    }

    /**
     * @Route("/admin/article/new", name="article_new", methods="GET|POST")
     *
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            /* @var Article $mainArticle */
            $mainArticle = $em->getRepository(Article::class)->findOneBy(['main' => true]);
            if (!is_null($mainArticle)) {
                $mainArticle->setMain(false);
                $em->persist($mainArticle);
            }

            $slug = str_replace(" ", "-", $article->getTitle());
            $uniqueSlugs = $em->getRepository(Article::class)->newSlug($slug);
            $em = $this->getDoctrine()->getManager();
            $i = count($uniqueSlugs);
            if ($i > 0) {
                $slug = $slug . "_" . $i++;
            }

            $user = $this->get('security.token_storage')->getToken()->getUser();
            $article->setUser($user);


            $article->setSlug($slug);
            $article->setPublished(new \DateTime("now"));
            $article->setMain(true);
            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('article_index');
        }

        return $this->render('article/new.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/article/{slug}", name="article_show", methods="GET")
     *
     * @param Article $article
     * @return Response
     */
    public function show(Article $article, CommentRepository $commentRepository): Response
    {
        $comment = new Comment();


        $form = $this->createForm(CommentType::class, $comment, [
            'action' => $this->generateUrl('comment_new', ['slug' => $article->getSlug()]),
            'method' => 'POST',
        ]);

        return $this->render('article/show.html.twig', ['article' => $article, 'form' => $form->createView()]);
    }

    /**
     * @Route("/admin/article/{slug}/edit", name="article_edit", methods="GET|POST")
     *
     * @param Request $request
     * @param Article $article
     * @return Response
     */
    public function edit(Request $request, Article $article): Response
    {
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('article_edit', ['slug' => $article->getSlug()]);
        }

        return $this->render('article/edit.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/article/{id}", name="article_delete", methods="DELETE")
     *
     * @param Request $request
     * @param Article $article
     * @return Response
     */
    public function delete(Request $request, Article $article): Response
    {
        if ($this->isCsrfTokenValid('delete' . $article->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($article);
            $em->flush();
        }

        return $this->redirectToRoute('article_index');
    }


    /**
     * @Route("/article/{id}/upvote", name="article_upvote")
     * @param $id
     * @return JsonResponse
     */
    public function upvote($id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var $article \App\Entity\Article */
        $article = $em->getRepository(Article::class)->find($id);

        if (!$article) {
            throw $this->createNotFoundException('No article found for id ' . $id);
        }
        /* @var User $currentUser */
        $currentUser = $this->get('security.token_storage')->getToken()->getUser();

        if (!$article->getUpvote()->contains($currentUser)) {
            $article->getUpvote()->add($currentUser);
            $em->persist($article);
            $em->flush();
        } else {
            $article->getUpvote()->removeElement($currentUser);
            $em->persist($article);
            $em->flush();
        }


        return new JsonResponse($article->getUpvote()->count());
    }


}
