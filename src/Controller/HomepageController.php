<?php
/**
 * Created by PhpStorm.
 * User: yoran
 * Date: 2-10-2018
 * Time: 08:57
 */

namespace App\Controller;

use App\Entity\Article;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class HomepageController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     */
    public function homepage()
    {
        $articles = $this->getDoctrine()
            ->getRepository(Article::class)->orderByDate();

        return $this->render("articles/home.html.twig", ["articles" => $articles]);
    }

    /**
     * @Route("/about-us", name="app_about_us")
     */
    public function about()
    {
        return $this->render("articles/about.html.twig");
    }
}