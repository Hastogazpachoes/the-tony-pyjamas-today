<?php
/**
 * Created by PhpStorm.
 * User: yoran
 * Date: 6-12-2018
 * Time: 12:32
 */

namespace App\Controller;


use App\Entity\Article;
use App\Entity\Comment;
use App\Entity\User;
use App\Form\CommentType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CommentController extends AbstractController
{
    /**
     * @Route("/article/{slug}/comment", name="comment_new", methods="POST")
     *
     * @param Request $request
     * @param Article $article
     * @return Response
     */
    public function comment(Request $request, Article $article): Response
    {
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);
        $retval = [];
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $comment->setArticle($article);
            /* @var User $currentUser */
            $currentUser = $this->get('security.token_storage')->getToken()->getUser();
            $comment->setUser($currentUser);

            $comment->setPublished(new \DateTime("now"));

            $em->persist($comment);
            $em->flush();
            $retval = ['username' => $comment->getUser()->getUsername(), 'date' => $comment->getPublished()->format('Y-m-d H:i'), 'comment' => $comment->getComment()];
        }


        return new JsonResponse($retval);
    }
}