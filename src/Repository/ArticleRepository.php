<?php

namespace App\Repository;

use App\Entity\Article;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Article::class);
    }

    /**
     * @return Article[] Returns an array of Article objects
     */
    public function orderByDate()
    {
        $query = $this->createQueryBuilder('a')
            ->orderBy('a.published', 'DESC');

        return $query->getQuery()->getResult();
    }

    /**
     * @param string $keyword
     * @return array
     */
    public function newSlug(string $keyword): array
    {
        $qb = $this->createQueryBuilder("a");

        $qb
            ->where($qb->expr()->like('a.slug', ':slug'))
            ->setParameter('slug', $keyword . '%');

        return $qb->getQuery()->getResult();
    }

    public function findByArticleUser(Article $article, User $user)
    {
        $qb = $this->createQueryBuilder("a");

        $qb
            ->innerJoin('a.upvote', 'uv', 'WITH', 'uv.user = :user')
            ->where($qb->expr()->like('a.id', ':article'))
            ->setParameter('article', $article->getId() )
            ->setParameter('user', $user);

        return $qb->getQuery()->getResult();
    }
}
