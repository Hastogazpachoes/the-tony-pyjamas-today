# The Tony Pyjamas Today CMS

Some stuff that Yoran needs to fill in...

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

- MariaDB needs to be installed
- PHP needs to be installed (or another way to run composer)

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
git clone git@gitlab.bluetea.nl:Siebers/TheTonyPyjamasToday.git .
```

Edit the .env file to match your database config

```
bin/console doctrine:database:create
```
```
bin/console doctrine:migrations:migrate
```
```
yarn install
```
```
yarn encore production
```
Run Composer
```
composer install
```
Run the application!

```
bin/console server:run
```
And you are good to go!

## Using the interface

You will need to register an account and use the cli tools to promote that user to ROLE_SUPER_ADMIN and again ROLE_ADMIN

```
bin/console fos:user:promote
```

## Running the tests

Sadly, no tests in this project


## Authors

* **Yoran Siebers** - *Initial work*


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
